package gnu.awt.virtual;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.peer.*;

public abstract class VirtualComponentPeer implements ComponentPeer // extends sun.awt.windows.PublicWComponentPeer// implements ComponentPeer
{
  // static VirtualToolkit defaultToolkit;
  public void dispose()
  {
  }
  
  // Cursor
  
  public void setCursor(Cursor cursor)
  {
  }
  
  //
  // Construction
  //
  
  public VirtualComponentPeer(Toolkit toolkit, Component component)
  {
    this.toolkit = toolkit;
    // super(component);
    this.component = component;
    setBounds(component.getX(), component.getY(), component.getWidth(), component.getHeight());
    
    // Disable double-buffering for Swing components
    javax.swing.RepaintManager.currentManager(component).setDoubleBufferingEnabled(false);
  }
  
  public Component getComponent()
  {
    return (component);
  }
  
  //
  // ComponentPeer
  //
  
  // Graphics
  
  public void paint(Graphics g)
  {
    // Log.MOBILE.debug("paint");
  }
  
  public void repaint(long tm, int x, int y, int width, int height)
  {
    // Log.MOBILE.debug("repaint");
  }
  
  public void print(Graphics g)
  {
  }
  
  public Graphics getGraphics()
  {
    Component parent = component.getParent();
    if (parent != null)
    {
      return parent.getGraphics().create(location.x, location.y, size.width, size.height);
    }
    else
      throw new Error();
  }
  
  public GraphicsConfiguration getGraphicsConfiguration()
  {
    return null;
  }
  
  // Bounds
  
  public void setBounds(int x, int y, int width, int height)
  {
    // Log.MOBILE.debug("setBounds "+x+","+y+","+width+","+height);
    size.width = width;
    size.height = height;
  }
  
  public Point getLocationOnScreen()
  {
    Point screen = new Point(location);
    Component parent = component.getParent();
    if (parent != null)
    {
      Point parentScreen = parent.getLocationOnScreen();
      screen.translate(parentScreen.x, parentScreen.y);
    }
    
    return screen;
  }
  
  public Dimension getPreferredSize()
  {
    return size;
  }
  
  public Dimension getMinimumSize()
  {
    return size;
  }
  
  // State
  
  public void setVisible(boolean b)
  {
  }
  
  public void setEnabled(boolean b)
  {
  }
  
  // Focus
  
  public void requestFocus()
  {
  }
  
  public boolean isFocusTraversable()
  {
    return true;
  }
  
  // Events
  
  public void handleEvent(AWTEvent e)
  {
  }
  
  public void coalescePaintEvent(PaintEvent e)
  {
  }
  
  // Color
  
  public ColorModel getColorModel()
  {
    return getToolkit().getColorModel();
  }
  
  public void setForeground(Color c)
  {
  }
  
  public void setBackground(Color c)
  {
  }
  
  // Fonts
  
  public FontMetrics getFontMetrics(Font font)
  {
    return null;
  }
  
  public void setFont(Font f)
  {
  }
  
  // Misc
  
  public Toolkit getToolkit()
  {
    // if( toolkit==null){
    // toolkit = defaultToolkit;
    // }
    if (toolkit == null)
    {
      toolkit = new VirtualToolkit();
    }
    return toolkit;
  }
  
  // Image
  
  public Image createImage(ImageProducer producer)
  {
    return null;
  }
  
  public Image createImage(int width, int height)
  {
    Component parent = component.getParent();
    if (parent != null)
      return parent.createImage(width, height);
    else
      throw new Error();
  }
  
  public boolean prepareImage(Image img, int w, int h, ImageObserver o)
  {
    return true;
  }
  
  public int checkImage(Image img, int w, int h, ImageObserver o)
  {
    return ImageObserver.ALLBITS;
  }
  
  // Deprecated
  
  public Dimension preferredSize()
  {
    return getPreferredSize();
  }
  
  public Dimension minimumSize()
  {
    return getMinimumSize();
  }
  
  public void show()
  {
    setVisible(true);
  }
  
  public void hide()
  {
    setVisible(false);
  }
  
  public void enable()
  {
    setEnabled(true);
  }
  
  public void disable()
  {
    setEnabled(false);
  }
  
  public void reshape(int x, int y, int width, int height)
  {
    setBounds(x, y, width, height);
  }
  
  public void reparent(ContainerPeer newContainer)
  {
    
  }
  
  // /////////////////////////////////////////////////////////////////////////////////////
  // Private
  
  protected Component component;
  protected Toolkit toolkit;
  protected Point location = new Point();
  protected Dimension size = new Dimension();
}
