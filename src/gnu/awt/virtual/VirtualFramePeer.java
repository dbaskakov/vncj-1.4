package gnu.awt.virtual;

import java.awt.*;
import java.awt.BufferCapabilities.*;
import java.awt.Rectangle;
import java.awt.image.*;
import java.awt.peer.*;

import sun.awt.CausedFocusEvent.*;
import sun.java2d.pipe.*;
import gnu.awt.*;

public class VirtualFramePeer extends VirtualComponentPeer implements FramePeer
{
  //
  // Construction
  //
  
  private Window getOwningFrameDialog(Window window)
  {
    while (window != null && !(window instanceof Frame ||
        window instanceof Dialog))
    {
      window = (Window) window.getParent();
    }
    return window;
  }
  
  public VirtualFramePeer(Frame frame, PixelsOwner pixelsOwner)
  {
    super(frame.getToolkit(), frame);
    this.pixelsOwner = pixelsOwner;
    // KeyboardFocusManager.setCurrentKeyboardFocusManager(new DefaultKeyboardFocusManager()
    // {
    // @Override
    // public boolean dispatchEvent(AWTEvent e)
    // {
    // if (e.getID() == WindowEvent.WINDOW_GAINED_FOCUS)
    // {
    // WindowEvent we = (WindowEvent) e;
    // Window oldFocusedWindow = getGlobalFocusedWindow();
    // Window newFocusedWindow = we.getWindow();
    // if (newFocusedWindow == oldFocusedWindow)
    // {
    // return true;
    // }
    // if (oldFocusedWindow != null)
    // {
    // oldFocusedWindow.dispatchEvent(new WindowEvent(oldFocusedWindow,
    // WindowEvent.WINDOW_LOST_FOCUS,
    // newFocusedWindow));
    //
    // }
    // Window newActiveWindow =
    // getOwningFrameDialog(newFocusedWindow);
    // Window currentActiveWindow = getGlobalActiveWindow();
    // if (newActiveWindow != currentActiveWindow)
    // {
    // newActiveWindow.dispatchEvent(
    // new WindowEvent(newActiveWindow,
    // WindowEvent.WINDOW_ACTIVATED,
    // currentActiveWindow));
    // }
    // setGlobalFocusedWindow(newFocusedWindow);
    // return true;
    // }
    // else
    // {
    // return super.dispatchEvent(e);
    // }
    // }
    // });
  }
  
  //
  // FramePeer
  //
  
  public void setTitle(String title)
  {
  }
  
  public void setIconImage(Image im)
  {
  }
  
  public void setMenuBar(MenuBar mb)
  {
    // TODO
  }
  
  public void setResizable(boolean resizeable)
  {
  }
  
  public void setState(int state)
  {
    // Always Frame.NORMAL
  }
  
  public int getState()
  {
    return Frame.NORMAL;
  }
  
  //
  // WindowPeer
  //
  
  public void toFront()
  {
    // Always "in front"
  }
  
  public void toBack()
  {
    // Always "in front"
  }
  
  //
  // ContainerPeer
  //
  
  public Insets getInsets()
  {
    return insets;
  }
  
  public void beginValidate()
  {
  }
  
  public void endValidate()
  {
  }
  
  public Insets insets()
  {
    return getInsets();
  }
  
  //
  // ComponentPeer
  //
  
  public void setVisible(boolean b)
  {
    if (b == true)
    {
      // First paint
      component.paint(getGraphics());
    }
  }
  
  public Graphics getGraphics()
  {
    // We will always be writing to the same image
    if (image == null)
    {
      // Thread.dumpStack();
      /*
       * if( size.width == 0 ) size.width = 100; if( size.height == 0 ) size.height = 100;
       */
      
      if ((size.width > 0) && (size.height > 0))
      {
        // Color model
        DirectColorModel colorModel = (DirectColorModel) getColorModel();
        
        // Pixel data
        int[] pixels = new int[size.width * size.height];
        DataBuffer dataBuffer = new DataBufferInt(pixels, pixels.length);
        
        // Sample model
        SampleModel sampleModel = new SinglePixelPackedSampleModel(DataBuffer.TYPE_INT, size.width, size.height, colorModel.getMasks());
        
        // Raster
        WritableRaster raster = Raster.createWritableRaster(sampleModel, dataBuffer, null);
        
        // Image
        image = new BufferedImage(colorModel, raster, true, null);
        
        // Set pixel owner
        pixelsOwner.setPixelArray(pixels, size.width, size.height);
      }
      else
      {
        return null;
      }
    }
    
    return image.getGraphics();
  }
  
  public Image createImage(int width, int height)
  {
    // Log.MOBILE.debug( "createImage("+width+","+height+")" );
    
    // Color model
    DirectColorModel colorModel = (DirectColorModel) getColorModel();
    
    // Sample model
    SampleModel sampleModel = new SinglePixelPackedSampleModel(DataBuffer.TYPE_INT, width, height, colorModel.getMasks());
    
    // Raster
    WritableRaster raster = Raster.createWritableRaster(sampleModel, null);
    
    // Image
    return new BufferedImage(colorModel, raster, true, null);
  }
  
  // /////////////////////////////////////////////////////////////////////////////////////
  // Private
  
  private PixelsOwner pixelsOwner;
  private Insets insets = new Insets(0, 0, 0, 0);
  private BufferedImage image = null;
  
  public void setMaximizedBounds(Rectangle rec)
  {
    // TODO:Need set
  }
  
  public void beginLayout()
  {
    
  }
  
  public void endLayout()
  {
    
  }
  
  public boolean isPaintPending()
  {
    return false;
  }
  
  public boolean canDetermineObscurity()
  {
    return false;
  }
  
  public void createBuffers(int arg0, BufferCapabilities arg1) throws AWTException
  {
    
  }
  
  public VolatileImage createVolatileImage(int arg0, int arg1)
  {
    return null;
  }
  
  public void destroyBuffers()
  {
    
  }
  
  public void flip(FlipContents arg0)
  {
    
  }
  
  public Image getBackBuffer()
  {
    return null;
  }
  
  public boolean handlesWheelScrolling()
  {
    return false;
  }
  
  public boolean isFocusable()
  {
    return false;
  }
  
  public boolean isObscured()
  {
    return false;
  }
  
  public boolean requestFocus(Component arg0, boolean arg1, boolean arg2, long arg3)
  {
    return false;
  }
  
  public void updateCursorImmediately()
  {
    
  }
  
  public void setBoundsPrivate(int x, int y, int width, int height)
  {
    
  }
  
  public boolean requestWindowFocus()
  {
    return false;
  }
  
  public void updateAlwaysOnTop()
  {
    
  }
  
  public void updateFocusableWindowState()
  {
    
  }
  
  public void cancelPendingPaint(int x, int y, int w, int h)
  {
    
  }
  
  public boolean isRestackSupported()
  {
    return false;
  }
  
  public void restack()
  {
    
  }
  
  public Rectangle getBounds()
  {
    return null;
  }
  
  public boolean isReparentSupported()
  {
    return false;
  }
  
  public void layout()
  {
    
  }
  
  public void setBounds(int x, int y, int width, int height, int op)
  {
    
  }
  
  @Override
  public Rectangle getBoundsPrivate()
  {
    return null;
  }
  
  @Override
  public void repositionSecurityWarning()
  {
    
  }
  
  public void updateAlwaysOnTopState()
  {
    
  }
  
  public void setAlwaysOnTop(boolean alwaysOnTop)
  {
    
  }
  
  @Override
  public void setModalBlocked(Dialog blocker, boolean blocked)
  {
    
  }
  
  @Override
  public void setOpacity(float opacity)
  {
    
  }
  
  @Override
  public void setOpaque(boolean isOpaque)
  {
    
  }
  
  @Override
  public void updateIconImages()
  {
    
  }
  
  @Override
  public void updateMinimumSize()
  {
    
  }
  
  @Override
  public void updateWindow()
  {
    
  }
  
  @Override
  public void applyShape(Region shape)
  {
    
  }
  
  public void setZOrder(ComponentPeer above)
  {
    // To change body of implemented methods use File | Settings | File Templates.
  }
  
  public boolean updateGraphicsData(GraphicsConfiguration gc)
  {
    return false; // To change body of implemented methods use File | Settings | File Templates.
  }
  
  @Override
  public void flip(int x1, int y1, int x2, int y2, FlipContents flipAction)
  {
    
  }
  
  @Override
  public boolean requestFocus(Component lightweightChild, boolean temporary,
      boolean focusedWindowChangeAllowed, long time, Cause cause)
  {
    return false;
  }
  
  public void resetImage()
  {
    image = null;
  }
}
