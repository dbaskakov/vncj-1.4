package gnu.awt.virtual;

import java.awt.*;
import java.awt.BufferCapabilities.*;
import java.awt.image.*;
import java.awt.peer.*;

import sun.awt.CausedFocusEvent.*;
import sun.java2d.pipe.*;

public class VirtualLightweightPeer extends VirtualComponentPeer implements LightweightPeer, ContainerPeer
{
  //
  // Construction
  //
  
  /**
   * new Constructor by Marcus Wolschon
   */
  public VirtualLightweightPeer(Toolkit toolkit, Component component)
  {
    super(toolkit, component);
  }
  
  /**
   * new Constructor by Marcus Wolschon
   */
  public VirtualLightweightPeer(Component component)
  {
    super(null, component);
  }
  
  public boolean canDetermineObscurity()
  {
    return false;
  }
  
  public void createBuffers(int arg0, BufferCapabilities arg1) throws AWTException
  {
    
  }
  
  public VolatileImage createVolatileImage(int arg0, int arg1)
  {
    return null;
  }
  
  public void destroyBuffers()
  {
    
  }
  
  public void flip(FlipContents arg0)
  {
    
  }
  
  public Image getBackBuffer()
  {
    return null;
  }
  
  public boolean handlesWheelScrolling()
  {
    return false;
  }
  
  public boolean isFocusable()
  {
    return false;
  }
  
  public boolean isObscured()
  {
    return false;
  }
  
  public boolean requestFocus(Component arg0, boolean arg1, boolean arg2, long arg3)
  {
    return false;
  }
  
  public void updateCursorImmediately()
  {
    
  }
  
  public Rectangle getBounds()
  {
    return null;
  }
  
  public boolean isReparentSupported()
  {
    return false;
  }
  
  public void layout()
  {
    
  }
  
  public void setBounds(int x, int y, int width, int height, int op)
  {
    
  }
  
  public Insets getInsets()
  {
    return insets;
  }
  
  public Insets insets()
  {
    return getInsets();
  }
  
  private Insets insets = new Insets(0, 0, 0, 0);
  
  public void beginLayout()
  {
    
  }
  
  public void beginValidate()
  {
    
  }
  
  public void cancelPendingPaint(int x, int y, int w, int h)
  {
    
  }
  
  public void endLayout()
  {
    
  }
  
  public void endValidate()
  {
    
  }
  
  public boolean isPaintPending()
  {
    return false;
  }
  
  public boolean isRestackSupported()
  {
    return false;
  }
  
  public void restack()
  {
    
  }
  
  @Override
  public void applyShape(Region shape)
  {
    
  }
  
  public void setZOrder(ComponentPeer above)
  {
    // To change body of implemented methods use File | Settings | File Templates.
  }
  
  public boolean updateGraphicsData(GraphicsConfiguration gc)
  {
    return false; // To change body of implemented methods use File | Settings | File Templates.
  }
  
  @Override
  public void flip(int x1, int y1, int x2, int y2, FlipContents flipAction)
  {
    
  }
  
  @Override
  public boolean requestFocus(Component lightweightChild,
      boolean temporary, boolean focusedWindowChangeAllowed,
      long time, Cause cause)
  {
    return false;
  }
  
}
