package gnu.awt.virtual;

import java.awt.*;
import java.awt.color.*;
import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.awt.dnd.peer.*;
import java.awt.im.*;
import java.awt.image.*;
import java.awt.peer.*;
import java.net.*;
import java.util.*;

import gnu.awt.*;

/**
 * we derive from sun.awt.SunToolkit because GlobalCursorManager depends on it in Windows if we are the default-toolkit.
 */
public class VirtualToolkit extends sun.awt.SunToolkit// Toolkit
{
  //
  // Static attributes
  //
  
  private static EventQueue eventQueue = new EventQueue();
  
  public static void initDefaultToolkit()
  {
    setDefaultToolkit();
    System.setProperty("awt.toolkit", VirtualToolkit.class.getName());
  }
  
  //
  // Construction
  //
  
  public VirtualToolkit(DirectColorModel colorModel, int width, int height)
  {
    super();
    
    if (defaultToolkit == null)
      setDefaultToolkit();
    
    this.colorModel = colorModel;
    
    screenSize = new Dimension(width, height);
  }
  
  public VirtualToolkit(int depth, int rMask, int gMask, int bMask, int width, int height)
  {
    this(new DirectColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB), depth, rMask, gMask, bMask, 0, true, DataBuffer.TYPE_INT), width, height);
  }
  
  public VirtualToolkit(int width, int height)
  {
    this(24, 0xFF0000, 0xFF00, 0xFF, width, height);
  }
  
  public VirtualToolkit(EventQueue eventQueue)
  {
    this(10000, 10000);
    this.eventQueue = eventQueue;
  }
  
  public VirtualToolkit()
  {
    this(10000, 10000);
  }
  
  //
  // Operations
  //
  
  public void setColorModel(DirectColorModel colorModel)
  {
    this.colorModel = colorModel;
  }
  
  //
  // Toolkit
  //
  
  // Peers
  
  @Override
  public ButtonPeer createButton(Button target)
  {
    return null;
  }
  
  @Override
  public CanvasPeer createCanvas(Canvas target)
  {
    return null;
  }
  
  @Override
  public CheckboxPeer createCheckbox(Checkbox target)
  {
    return null;
  }
  
  @Override
  public CheckboxMenuItemPeer createCheckboxMenuItem(CheckboxMenuItem target)
  {
    return null;
  }
  
  @Override
  public ChoicePeer createChoice(Choice target)
  {
    return null;
  }
  
  @Override
  protected LightweightPeer createComponent(Component target)
  {
    return new VirtualLightweightPeer(this, target); // using new constructor by Marcus Wolschon
  }
  
  @Override
  public DialogPeer createDialog(Dialog target)
  {
    return null;
  }
  
  @Override
  public DragSourceContextPeer createDragSourceContextPeer(DragGestureEvent dge)
  {
    return null;
  }
  
  @Override
  public FileDialogPeer createFileDialog(FileDialog target)
  {
    return null;
  }
  
  @Override
  public FramePeer createFrame(Frame target)
  {
    
    if (!(target instanceof PixelsOwner))
    {
      // test throw new Error( "Virtual toolkit does not support this frame - it is not a PixelsOwner" );
      return new VirtualFramePeer(target, null);
    }
    
    return new VirtualFramePeer(target, (PixelsOwner) target);
  }
  
  @Override
  public LabelPeer createLabel(Label target)
  {
    return null;
  }
  
  @Override
  public ListPeer createList(java.awt.List target)
  {
    return null;
  }
  
  @Override
  public MenuPeer createMenu(Menu target)
  {
    return null;
  }
  
  @Override
  public MenuBarPeer createMenuBar(MenuBar target)
  {
    return null;
  }
  
  @Override
  public MenuItemPeer createMenuItem(MenuItem target)
  {
    return null;
  }
  
  @Override
  public PanelPeer createPanel(Panel target)
  {
    return null;
  }
  
  @Override
  public PopupMenuPeer createPopupMenu(PopupMenu target)
  {
    return null;
  }
  
  @Override
  public ScrollbarPeer createScrollbar(Scrollbar target)
  {
    return null;
  }
  
  @Override
  public ScrollPanePeer createScrollPane(ScrollPane target)
  {
    return null;
  }
  
  @Override
  public TextAreaPeer createTextArea(TextArea target)
  {
    return null;
  }
  
  @Override
  public TextFieldPeer createTextField(TextField target)
  {
    return null;
  }
  
  @Override
  public WindowPeer createWindow(Window target)
  {
    return null;
  }
  
  // Images
  
  @Override
  public int checkImage(Image image, int width, int height, ImageObserver observer)
  {
    return defaultToolkit.checkImage(image, width, height, observer);
  }
  
  @Override
  public Image createImage(byte[] imagedata, int imageoffset, int imagelength)
  {
    return defaultToolkit.createImage(imagedata, imageoffset, imagelength);
  }
  
  @Override
  public Image createImage(ImageProducer producer)
  {
    return defaultToolkit.createImage(producer);
  }
  
  @Override
  public Image createImage(String filename)
  {
    return defaultToolkit.createImage(filename);
  }
  
  @Override
  public Image createImage(URL url)
  {
    return defaultToolkit.createImage(url);
  }
  
  @Override
  public Image getImage(String filename)
  {
    return defaultToolkit.getImage(filename);
  }
  
  @Override
  public Image getImage(URL url)
  {
    return defaultToolkit.getImage(url);
  }
  
  @Override
  public boolean prepareImage(Image image, int width, int height, ImageObserver observer)
  {
    return defaultToolkit.prepareImage(image, width, height, observer);
  }
  
  // Color
  
  @Override
  public ColorModel getColorModel()
  {
    return colorModel;
  }
  
  // Fonts
  
  @Override
  public String[] getFontList()
  {
    return defaultToolkit.getFontList();
  }
  
  @Override
  public FontMetrics getFontMetrics(Font font)
  {
    return defaultToolkit.getFontMetrics(font);
  }
  
  @Override
  public FontPeer getFontPeer(String name, int style)
  {
    return null;
    // protected: return defaultToolkit.getFontPeer( name, style );
  }
  
  // Other
  
  @Override
  public void beep()
  {
    // Quiet, please!
  }
  
  @Override
  public PrintJob getPrintJob(Frame frame, String jobtitle, Properties prop)
  {
    return null;
  }
  
  @Override
  public int getScreenResolution()
  {
    return -1;
  }
  
  @Override
  public Dimension getScreenSize()
  {
    return screenSize;
  }
  
  @Override
  public Clipboard getSystemClipboard()
  {
    // TODO: virtual clipboard
    return null;
  }
  
  @Override
  protected EventQueue getSystemEventQueueImpl()
  {
    return eventQueue;
    // return defaultToolkit.getSystemEventQueue(); //TODO: try using our own instance of EventQueue
    // return Toolkit.getDefaultToolkit().getSystemEventQueue();
    // protected: return defaultToolkit.getSystemEventQueueImpl();
  }
  
  protected boolean syncNativeQueue(long l)
  {
    return false; // To change body of implemented methods use File | Settings | File Templates.
  }
  
  public EventQueue getTheEventQueue()
  {
    // Hack Hack hack
    return eventQueue;
  }
  
  @Override
  public Map mapInputMethodHighlight(InputMethodHighlight highlight)
  {
    return null;
  }
  
  @Override
  public void sync()
  {
  }
  
  // /////////////////////////////////////////////////////////////////////////////////////
  // from sun.awt.SunToolkit
  
  // public sun.awt.GlobalCursorManager getGlobalCursorManager()
  // {
  // return defaultToolkit.getGlobalCursorManager();
  // }
  
  // public String getDefaultUnicodeEncoding()
  // {
  // return defaultToolkit.getDefaultUnicodeEncoding();
  // }
  
  @Override
  public java.awt.im.spi.InputMethodDescriptor getInputMethodAdapterDescriptor() throws AWTException
  {
    return defaultToolkit.getInputMethodAdapterDescriptor();
  }
  
  @Override
  public RobotPeer createRobot(Robot p0, GraphicsDevice p1) throws AWTException
  {
    return defaultToolkit.createRobot(p0, p1);
  }
  
  public KeyboardFocusManagerPeer createKeyboardFocusManagerPeer(KeyboardFocusManager keyboardFocusManager) throws HeadlessException
  {
    return null;
  }
  
  @Override
  protected int getScreenWidth()
  {
    return getScreenSize().width;
  }
  
  @Override
  protected int getScreenHeight()
  {
    return getScreenSize().height;
  }
  
  // /////////////////////////////////////////////////////////////////////////////////////
  // Private
  
  protected static sun.awt.SunToolkit defaultToolkit = null;
  private DirectColorModel colorModel;
  private final Dimension screenSize;
  
  private static void setDefaultToolkit()
  {
    // System.out.println(System.getProperty("awt.toolkit"));
    // if(System.getProperty("awt.toolkit","sun.awt.motif.MToolkit").endsWith("VirtualToolkit"))
    // {
    boolean notfound = true;
    String configuredDefaultToolkit = System.getProperty("gnu.awt.virtual.VirtualToolkit.OldDefaultToolkit");
    if (configuredDefaultToolkit != null)
      try
      {
        defaultToolkit = (sun.awt.SunToolkit) Class.forName(configuredDefaultToolkit).newInstance();
        return;
      }
      catch (Exception x)
      {
      }
    try
    {
      defaultToolkit = (sun.awt.SunToolkit) Class.forName("sun.awt.X11.XToolkit").newInstance();
      return;
    }
    catch (Exception x)
    {
    }
    try
    {
      defaultToolkit = (sun.awt.SunToolkit) Class.forName("sun.awt.windows.WToolkit").newInstance();
      return;
    }
    catch (Exception x)
    {
    }
    defaultToolkit = null;
    if (notfound)
      
      // }
      // else
      defaultToolkit = (sun.awt.SunToolkit) Toolkit.getDefaultToolkit(); // this would cause an infinite loop if we are the default-toolkit
    // ZLi changes
    // sun.awt.SunToolkit.lastMetrics = defaultToolkit.lastMetrics;
    
    /*
     * try { Class defaultToolkitClass = Class.forName( System.getProperty( "awt.toolkit" ) ); VirtualToolkit.defaultToolkit = (Toolkit) defaultToolkitClass.newInstance(); } catch( Exception x ) {
     * x.printStackTrace(); }
     * 
     * loadAssistive();
     */
  }
  
  public String getDefaultCharacterEncoding()
  {
    return null;
  }
  
  @Override
  public SystemTrayPeer createSystemTray(SystemTray arg0)
  {
    return null;
  }
  
  @Override
  public TrayIconPeer createTrayIcon(TrayIcon arg0) throws HeadlessException, AWTException
  {
    return null;
  }
  
  @Override
  public void grab(Window arg0)
  {
    
  }
  
  @Override
  public boolean isDesktopSupported()
  {
    return false;
  }
  
  @Override
  public boolean isTraySupported()
  {
    return false;
  }
  
  public boolean isWindowOpacityControlSupported()
  {
    return false;
  }
  
  @Override
  public boolean isWindowShapingSupported()
  {
    return false;
  }
  
  @Override
  public boolean isWindowTranslucencySupported()
  {
    return false;
  }
  
  protected boolean syncNativeQueue()
  {
    return false;
  }
  
  @Override
  public void ungrab(Window arg0)
  {
    
  }
  
  @Override
  protected DesktopPeer createDesktopPeer(Desktop target) throws HeadlessException
  {
    return null;
  }
  
  public KeyboardFocusManagerPeer getKeyboardFocusManagerPeer() throws HeadlessException
  {
    return null;
  }
  
  /*
   * private static void loadAssistive() { final String sep = File.separator; final Properties properties = new Properties();
   * 
   * java.security.AccessController.doPrivileged( new java.security.PrivilegedAction() { public Object run() { try { File propsFile = new File( System.getProperty("java.home") + sep + "lib" + sep +
   * "accessibility.properties"); FileInputStream in = new FileInputStream(propsFile); properties.load(new BufferedInputStream(in)); in.close(); } catch (Exception e) { // File does not exist; no
   * classes will be auto loaded } return null; } } );
   * 
   * String atNames = properties.getProperty("assistive_technologies",null); ClassLoader cl = ClassLoader.getSystemClassLoader();
   * 
   * if (atNames != null) { StringTokenizer parser = new StringTokenizer(atNames," ,"); String atName; while (parser.hasMoreTokens()) { atName = parser.nextToken(); try { Class clazz; if (cl != null)
   * { clazz = cl.loadClass(atName); } else { clazz = Class.forName(atName); } clazz.newInstance(); } catch (ClassNotFoundException e) { throw new AWTError("Assistive Technology not found: " +
   * atName); } catch (InstantiationException e) { throw new AWTError("Could not instantiate Assistive" + " Technology: " + atName); } catch (IllegalAccessException e) { throw new
   * AWTError("Could not access Assistive" + " Technology: " + atName); } catch (Exception e) { throw new AWTError("Error trying to install Assistive" + " Technology: " + atName + " " + e); } } } }
   */
}
