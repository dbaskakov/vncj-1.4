package vncjdemo;

import javax.swing.*;

import gnu.awt.virtual.*;
import gnu.vnc.awt.*;

public class SwingModelTest extends VNCJFrame
{
  //
  // Construction
  //
  
  public SwingModelTest(int display, String displayName, int width, int height)
  {
    super(new VirtualToolkit(), displayName, width, height);
    
    desktopPane = new JDesktopPane();
    getContentPane().add(desktopPane);
    
    addFrame();
    addFrame();
    // Create frame
    JInternalFrame frame = new JInternalFrame("Editor [" + openFrameCount + "]", true, false, true, true);
    desktopPane.add(frame);
    
    // Populate it
    JComboBox cb = new JComboBox(new String[] { "One", "Two", "Three" });
    frame.getContentPane().add(cb);
    
    // Show it
    frame.setLocation(xOffset * openFrameCount, yOffset * openFrameCount++);
    frame.setSize(300, 200);
    frame.setVisible(true);
  }
  
  // /////////////////////////////////////////////////////////////////////////////////////
  // Private
  
  private JDesktopPane desktopPane;
  private int openFrameCount = 0;
  private int xOffset = 30, yOffset = 30;
  
  private void addFrame()
  {
    // Create frame
    JInternalFrame frame = new JInternalFrame("Editor [" + openFrameCount + "]", true, false, true, true);
    desktopPane.add(frame);
    
    // Populate it
    JTextArea text = new JTextArea("This is a Swing JInternalFrame\ncontaining a Swing JTextArea.");
    frame.getContentPane().add(new JScrollPane(text));
    
    // Show it
    frame.setLocation(xOffset * openFrameCount, yOffset * openFrameCount++);
    frame.setSize(300, 200);
    frame.setVisible(true);
  }
}
