package vncjdemo;

import java.awt.*;
import java.awt.event.*;
import java.awt.peer.*;
import java.lang.reflect.*;

public class VirtualKeyboardFocusManager extends DefaultKeyboardFocusManager
{
  public VirtualKeyboardFocusManager()
  {
    Field peerField = null;
    
    for (Field f : KeyboardFocusManager.class.getDeclaredFields())
    {
      if ("peer".equals(f.getName()))
      {
        peerField = f;
        break;
      }
    }
    if (peerField != null)
      try
      {
        peerField.setAccessible(true);
        peerField.set(this, new FakePeer());
      }
      catch (IllegalArgumentException e)
      {
        e.printStackTrace();
      }
      catch (IllegalAccessException e)
      {
        e.printStackTrace();
      }
  }
  
  @Override
  public boolean dispatchEvent(AWTEvent e)
  {
    if (e.getID() == WindowEvent.WINDOW_GAINED_FOCUS)
    {
      WindowEvent we = (WindowEvent) e;
      Window oldFocusedWindow = getGlobalFocusedWindow();
      Window newFocusedWindow = we.getWindow();
      if (newFocusedWindow == oldFocusedWindow)
      {
        return true;
      }
      if (oldFocusedWindow != null)
      {
        oldFocusedWindow.dispatchEvent(new WindowEvent(oldFocusedWindow,
            WindowEvent.WINDOW_LOST_FOCUS,
            newFocusedWindow));
        
      }
      Window newActiveWindow =
          getOwningFrameDialog1(newFocusedWindow);
      Window currentActiveWindow = getGlobalActiveWindow();
      if (newActiveWindow != currentActiveWindow)
      {
        newActiveWindow.dispatchEvent(
            new WindowEvent(newActiveWindow,
                WindowEvent.WINDOW_ACTIVATED,
                currentActiveWindow));
      }
      setGlobalFocusedWindow(newFocusedWindow);
      return true;
    }
    else
    {
      return super.dispatchEvent(e);
    }
    
  }
  
  private Window getOwningFrameDialog1(Window window)
  {
    while (window != null && !(window instanceof Frame ||
        window instanceof Dialog))
    {
      window = (Window) window.getParent();
    }
    return window;
    
  }
  
  private static class FakePeer
      implements KeyboardFocusManagerPeer
  {
    Window win;
    Component comp;
    
    public void setCurrentFocusedWindow(Window win)
    {
      this.win = win;
    }
    
    public Window getCurrentFocusedWindow()
    {
      return win;
    }
    
    public void setCurrentFocusOwner(Component comp)
    {
      this.comp = comp;
    }
    
    public Component getCurrentFocusOwner()
    {
      return comp;
    }
    
    public void clearGlobalFocusOwner(Window activeWindow)
    {
    }
  }
}

/*
 * Location: /var/lib/ignition/data/mobile_wdir/mobile-client-bootstrap.jar Qualified Name: com.inductiveautomation.mobile.VirtualKeyboardFocusManager JD-Core Version: 0.6.2
 */
